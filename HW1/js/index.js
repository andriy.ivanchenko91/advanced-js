class Employee {
  constructor(options) {
    this._name = options.name;
    this._age = options.age;
    this._salary = options.salary;
  }

  get name() {
    return `Employee's name is ${this._name}`;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return `Employee's salary is ${this._salary}`;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(options) {
    super(options);
    this.lang = options.lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const vasya = new Employee({
  name: "Andriy",
  age: "30",
  salary: "3000",
  lang: { backend: "PHP", frontend: "javascript" },
});

const roman = new Programmer({
  name: "Roman",
  age: "40",
  salary: "4000",
  lang: {
    backend: "C#",
  },
});

const alex = new Programmer({
  name: "Alexei",
  age: "25",
  salary: "5000",
  lang: { backend: "C#", frontend: "Typescript" },
});

const gennadiy = new Programmer({
  name: "Gena",
  age: "21",
  salary: "1500",
  lang: { backend: "Java", frontend: "Javacript" },
});

console.log(roman);
console.log(alex);
console.log(gennadiy);
